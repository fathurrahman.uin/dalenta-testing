package kedaiapps.projeku.dalentatesting.helper

import kedaiapps.projeku.dalentatesting.R

class DataHome (
    val id: Int = 0,
    val image: Int = 0,
    val name: String = "",
    val price: Int = 0
) {

    fun getData() : List<DataHome> {
        val data : MutableList<DataHome> = ArrayList()
        data.add(DataHome(
            1,
            R.drawable.dummy_menu,
            "Banana Cake",
            20000
        ))
        data.add(DataHome(
            2,
            R.drawable.dummy_menu,
            "Chicken Crispy",
            25000
        ))
        data.add(DataHome(
            3,
            R.drawable.dummy_menu,
            "Pizza",
        30000
        ))
        data.add(DataHome(
            4,
            R.drawable.dummy_menu,
            "Rice Bowl",
            35000
        ))
        data.add(DataHome(
            5,
            R.drawable.dummy_menu,
            "Chicken Teriaki",
            40000
        ))
        data.add(DataHome(
            6,
            R.drawable.dummy_menu,
            "Ramen",
            45000
        ))
        data.add(DataHome(
            7,
            R.drawable.dummy_menu,
            "Mie Curry Udon",
            55000
        ))
        data.add(DataHome(
            8,
            R.drawable.dummy_menu,
            "Beef",
            60000
        ))

        return data
    }
}