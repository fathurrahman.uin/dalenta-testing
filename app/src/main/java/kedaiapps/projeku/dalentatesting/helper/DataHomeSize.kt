package kedaiapps.projeku.dalentatesting.helper

class DataHomeSize (
    val title: String = "",
    val price: Int = 0
) {

    fun getData() : List<DataHomeSize> {
        val data : MutableList<DataHomeSize> = ArrayList()
        data.add(DataHomeSize(
            "Little",
            0
        ))
        data.add(DataHomeSize(
            "Half",
            10000
        ))
        data.add(DataHomeSize(
            "Whole",
            20000
        ))
        return data
    }
}