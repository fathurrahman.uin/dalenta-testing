package kedaiapps.projeku.dalentatesting.utils

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.widget.ImageView
import coil.api.load
import com.theartofdev.edmodo.cropper.CropImage
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.text.SimpleDateFormat
import java.util.*

//fun openImageChooser(context: Context, activity: Activity, codeRequest : Int) {
//    val intent = Intent(context, AlbumSelectActivity::class.java)
//    intent.putExtra(Constants.INTENT_EXTRA_IMAGES, 10)
//    activity.startActivityForResult(intent, codeRequest)
//}

inline fun handleImageChooserResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?,
    onResult: (Uri?, error: String?) -> Unit
) {
    if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
        val result = CropImage.getActivityResult(data)
        if (resultCode == Activity.RESULT_OK) {
            onResult(result.uri, null)
        } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            onResult(null, result.error.toString())
        }
    }
}

fun compressImage(imageFile: File, destWidth: Int = 1080): File  {
    val b = BitmapFactory.decodeFile(imageFile.absolutePath)

    val origWidth = b.width
    val origHeight = b.height

    if (origWidth > destWidth) {
        val destHeight = origHeight / (origWidth / destWidth.toFloat())
        val b2 = Bitmap.createScaledBitmap(b, destWidth, destHeight.toInt(), false)
        val outStream = ByteArrayOutputStream()
        b2.compress(Bitmap.CompressFormat.JPEG, 70, outStream)
        val fileName = imageFile.absolutePath.replace(
            imageFile.name,
            "${imageFile.nameWithoutExtension}-compressed.${imageFile.extension}"
        )
        val f = File(fileName)
        f.createNewFile()
        //write the bytes in file
        val fo = FileOutputStream(f)
        fo.write(outStream.toByteArray())
        // remember close de FileOutput
        fo.close()

        b.recycle()
        b2.recycle()
        return f
    }

    b.recycle()
    return imageFile
}

fun ImageView.loadUrlImage(uri: String?) {
    val toHttps :String? = uri?.replace("http://", "https://")
    this.load(toHttps) {
        crossfade(true)
    }
}


fun getDateNow():String{
    val sdf = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
    val now = Date()
    return sdf.format(now)
}