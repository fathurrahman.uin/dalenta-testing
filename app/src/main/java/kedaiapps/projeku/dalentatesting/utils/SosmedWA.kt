package kedaiapps.projeku.dalentatesting.utils

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.widget.Toast

class SosmedWA {

    fun whatApps(context: Context, contact: String, total: String, change: String){
//        val contact = "+6281222602338" // use country code with your phone number
        val text =
                    "Total belanja anda $total.\n" +
                    "Total kembalian anda $change.\n" +
                    "Terima kasih." // use country code with your phone number

        val url = "https://api.whatsapp.com/send?phone=$contact&text=$text"

        try {
            val pm: PackageManager = context.packageManager
            pm.getPackageInfo("com.whatsapp", PackageManager.GET_ACTIVITIES)

            val intent = Intent(Intent.ACTION_VIEW)
            intent.data = Uri.parse(url)
            context.startActivity(intent)
        } catch (e: PackageManager.NameNotFoundException) {
            Toast.makeText(
                context,
                "Whatsapp app not installed in your phone",
                Toast.LENGTH_LONG
            ).show()
            e.printStackTrace()
        }
    }
}