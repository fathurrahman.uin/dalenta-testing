package kedaiapps.projeku.dalentatesting

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class Dalenta : Application()