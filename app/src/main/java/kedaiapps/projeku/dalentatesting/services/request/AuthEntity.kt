package kedaiapps.projeku.dalentatesting.services.request

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

//login
@Keep
data class LoginBody(
    @SerializedName("userid") val email: String,
    @SerializedName("userpwd") val password: String,
    @SerializedName("fbtoken") val fcm_token: String
)

//register
@Keep
data class RegisterBody(
    @SerializedName("name") val name: String,
    @SerializedName("phone") val phone: String,
    @SerializedName("email") val email: String,
    @SerializedName("password") val password: String
)

//activation
@Keep
data class ActivationBody(
    @SerializedName("active_number") val active_number: String
)
