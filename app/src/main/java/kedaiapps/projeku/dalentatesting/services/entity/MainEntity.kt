package kedaiapps.projeku.dalentatesting.services.entity

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

@Keep
data class ResponseHome(
        @SerializedName("id") val id: String,
        @SerializedName("image") val image: String,
        @SerializedName("name") val name: String,
        @SerializedName("price") val price: String,
)

@Keep
data class ResponseHomeProduct(
        @SerializedName("id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("qty") val qty: Int,
        @SerializedName("size") val size: String,
        @SerializedName("price") val price: Int,
        @SerializedName("note") val note: String,
        @SerializedName("service") val service: Boolean,
        @SerializedName("tax") val tax: Boolean,
)