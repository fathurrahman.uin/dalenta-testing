package kedaiapps.projeku.dalentatesting.services.rest

import kedaiapps.projeku.dalentatesting.services.Response
import kedaiapps.projeku.dalentatesting.services.ResponseAuth
import kedaiapps.projeku.dalentatesting.services.ResponseRegister
import kedaiapps.projeku.dalentatesting.services.request.ActivationBody
import kedaiapps.projeku.dalentatesting.services.request.LoginBody
import kedaiapps.projeku.dalentatesting.services.request.RegisterBody
import retrofit2.http.*

interface AuthRest {

    @Headers("@: Auth")
    @POST("login")
    suspend fun login(@Body param: LoginBody) : Response<ResponseAuth>

    @POST("register/member")
    suspend fun register(@Body param : RegisterBody) : ResponseRegister

    @POST("activation/member")
    suspend fun activation(@Body param : ActivationBody) : ResponseAuth
}