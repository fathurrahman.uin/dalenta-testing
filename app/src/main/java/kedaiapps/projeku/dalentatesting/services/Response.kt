package kedaiapps.projeku.dalentatesting.services
import androidx.annotation.Keep

import com.google.gson.annotations.SerializedName


class Response<T> (
    @SerializedName("success") val success: Boolean,
    @SerializedName("message") val message: String,
    @SerializedName("data") val data: T
)

@Keep
data class SuccessResponse(
    @SerializedName("success") val success: Boolean,
    @SerializedName("message") val message: String
)

@Keep
data class ErrorResponse(
    @SerializedName("status") val status: Boolean,
    @SerializedName("message") val message: String
)

@Keep
data class ResponseAuth(
    @SerializedName("merchantid") val merchantid: String,
    @SerializedName("merchant") val merchant: String,
    @SerializedName("userlevel") val userlevel: String,
    @SerializedName("token") val token: String
)

@Keep
data class ResponseRegister(
    @SerializedName("success") val success: Boolean,
    @SerializedName("message") val message: String,
    @SerializedName("active_number") val active_number: Int
)
