package kedaiapps.projeku.dalentatesting.viewmodel

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kedaiapps.projeku.dalentatesting.common.storage.Preferences
import javax.inject.Inject

@HiltViewModel
class RepoViewModel @Inject constructor(
    private val preferences: Preferences,
) : ViewModel() {

    fun setPrinter(printer: Int){
        preferences.printer = printer
    }

    fun setPrinterName(printername: String){
        preferences.printername = printername
    }

    fun getPrinter() = preferences.printer
    fun getPrinterName() = preferences.printername

    fun getMerchant() = preferences.merchant
    fun getUserLevel() = preferences.userlevel
    fun Logout() = preferences.clear()
}