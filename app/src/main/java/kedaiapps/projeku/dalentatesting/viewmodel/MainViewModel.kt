package kedaiapps.projeku.dalentatesting.viewmodel

import android.app.Application
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kedaiapps.projeku.dalentatesting.common.ActionLiveData
import kedaiapps.projeku.dalentatesting.common.UiState
import kedaiapps.projeku.dalentatesting.common.storage.Preferences
import kedaiapps.projeku.dalentatesting.db.DalentaDB
import kedaiapps.projeku.dalentatesting.db.table.DalentaTable
import kedaiapps.projeku.dalentatesting.ext.errorMesssage
import kedaiapps.projeku.dalentatesting.ext.ioThread
import kedaiapps.projeku.dalentatesting.services.request.LoginBody
import kedaiapps.projeku.dalentatesting.services.rest.AuthRest
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val authRest: AuthRest,
    private val preferences: Preferences,
    val application: Application
): ViewModel(){
    val db = DalentaDB.getDB(this.application)

    val authState = ActionLiveData<UiState>()

    fun login(userid: String, pass: String, fbtoken: String){
        viewModelScope.launch {
            authState.sendAction(UiState.Loading)
            try {
                val param = LoginBody(userid, pass, fbtoken)
                val response = authRest.login(param)
                if(response.success){
                    // set session login
                    preferences.token = response.data.token
                    preferences.merchantid = response.data.merchantid
                    preferences.merchant = response.data.merchant
                    preferences.userlevel = response.data.userlevel

                    authState.sendAction(UiState.Success)
                }else{
                    authState.sendAction(UiState.Error(response.message))
                }
            }catch (e: Exception){
                authState.sendAction(UiState.Error(e.errorMesssage))
            }
        }
    }

    //local db
    fun insertDalenta(subid:Int, name: String, price: Int, qty: Int, size: String, sizeprice: Int, note: String, total: Int, service: Boolean, tax: Boolean){
        ioThread {
            db.daoDalenta().insert(DalentaTable(0, subid, name, price, qty, size, sizeprice, note, total, service, tax))
        }
    }

    fun getDalenta() : LiveData<List<DalentaTable>> {
        return db.daoDalenta().getAll()
    }

    fun getDalentaBySize(subid: Int, size: String) : LiveData<DalentaTable>{
        return db.daoDalenta().getBySize(subid, size)
    }

    fun updateDalenta(subid: Int, qty: Int, size: String, sizeprice: Int, note: String, total: Int, service: Boolean, tax: Boolean){
        ioThread {
            db.daoDalenta().update(subid, qty, size, sizeprice, note, total, service, tax)
        }
    }

    fun deleteById(id: Int){
        ioThread {
            db.daoDalenta().deleteById(id)
        }
    }

    fun deleteAll(){
        ioThread {
            db.daoDalenta().deleteAll()
        }
    }
}