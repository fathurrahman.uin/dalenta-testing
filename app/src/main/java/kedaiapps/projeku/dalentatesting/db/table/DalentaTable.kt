package kedaiapps.projeku.dalentatesting.db.table

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class DalentaTable (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val subid: Int,
    val name: String,
    val price: Int,
    val qty: Int,
    val size: String,
    val sizeprice: Int,
    val note: String,
    val total: Int,
    val service: Boolean,
    val tax: Boolean,
)