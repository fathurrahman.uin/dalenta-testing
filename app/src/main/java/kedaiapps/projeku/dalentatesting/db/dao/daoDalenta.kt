package kedaiapps.projeku.dalentatesting.db.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kedaiapps.projeku.dalentatesting.db.table.DalentaTable

@Dao
interface daoDalenta {
    @Query("SELECT * FROM DalentaTable")
    fun getAll(): LiveData<List<DalentaTable>>

    @Query("SELECT * FROM DalentaTable WHERE subid=:subid and size=:size")
    fun getBySize(subid:Int, size: String): LiveData<DalentaTable>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(vararg data: DalentaTable)

    @Query("UPDATE DalentaTable SET qty=:qty, size=:size, sizeprice=:sizeprice, note=:note, total=:total, service=:service, tax=:tax WHERE subid=:subid")
    fun update(subid: Int, qty: Int, size: String, sizeprice: Int, note: String, total: Int, service: Boolean, tax: Boolean)

    @Query("DELETE FROM DalentaTable")
    fun deleteAll()

    @Query("DELETE FROM DalentaTable WHERE id=:id")
    fun deleteById(id: Int)
}