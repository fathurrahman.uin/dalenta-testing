package kedaiapps.projeku.dalentatesting.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kedaiapps.projeku.dalentatesting.db.dao.daoDalenta
import kedaiapps.projeku.dalentatesting.db.table.DalentaTable

@Database(entities = [DalentaTable::class], version = 3, exportSchema = false)
abstract class DalentaDB: RoomDatabase(){
    companion object{
        private var instance: DalentaDB? = null

        fun getDB(context: Context): DalentaDB{
            if(instance == null){
                instance = Room.databaseBuilder(context.applicationContext, DalentaDB::class.java, "DalentaDB")
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance as DalentaDB
        }
    }

    abstract fun daoDalenta(): daoDalenta
}
