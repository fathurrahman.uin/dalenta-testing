package kedaiapps.projeku.dalentatesting.ui.main.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kedaiapps.projeku.dalentatesting.R
import kedaiapps.projeku.dalentatesting.databinding.ItemHomeProductsBinding
import kedaiapps.projeku.dalentatesting.db.table.DalentaTable
import kedaiapps.projeku.dalentatesting.ext.inflate
import kedaiapps.projeku.dalentatesting.ext.toRupiah

class AdapterHomeProduct (
    private val onClick:(DalentaTable) -> Unit,
    private val onClickDel:(DalentaTable) -> Unit,
) : RecyclerView.Adapter<AdapterHomeProduct.ViewHolder>() {

    var items: MutableList<DalentaTable> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_home_products))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as? ViewHolder)?.bind(items.getOrNull(position)!!)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView){
        private var binding = ItemHomeProductsBinding.bind(containerView)

        fun bind(data: DalentaTable){
            with(binding){

                var jml=""
                if(data.qty > 1){
                    jml = "x"+data.qty
                }

                tvProduct.text = data.name+" "+jml
                tvItem.text = data.size
                tvPrice.text = data.total.toString().toRupiah()

                edit.setOnClickListener {
                    onClick(data)
                }

                delete.setOnClickListener {
                    onClickDel(data)
                }
            }
        }
    }

    fun insertData(data: List<DalentaTable>){
        data.forEach {
            items.add(it)
            notifyDataSetChanged()
        }
    }

    fun clearData() {
        if (items.isNotEmpty()) {
            items.clear()
            notifyDataSetChanged()
        }
    }
}