package kedaiapps.projeku.dalentatesting.ui.main

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kedaiapps.projeku.dalentatesting.R
import kedaiapps.projeku.dalentatesting.databinding.FragmentReceiptBinding
import kedaiapps.projeku.dalentatesting.ext.toRupiah
import kedaiapps.projeku.dalentatesting.modules.base.BaseFragment
import kedaiapps.projeku.dalentatesting.utils.SosmedWA
import kedaiapps.projeku.dalentatesting.viewmodel.MainViewModel

class ReceiptFragment : BaseFragment() {

    lateinit var mBinding: FragmentReceiptBinding
    private val viewModelMain by viewModels<MainViewModel>()
    private val args by navArgs<ReceiptFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentReceiptBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initView()
    }

    private fun initToolbar() {
        mBinding.line.setOnClickListener {
            viewModelMain.deleteAll()
            findNavController().navigate(R.id.homeFragment)
        }

        activity?.onBackPressedDispatcher?.addCallback(requireActivity(),
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().navigate(R.id.homeFragment)
//                    Toast.makeText(requireContext(), "Tombol ini tidak bisa digunakan", Toast.LENGTH_SHORT).show()
                }
            }
        )
    }

    private fun initView() {

        val jml = args.pay - args.total
        if(jml == 0){
            mBinding.change.text = "No Change"
        }else {
            mBinding.change.text = jml.toString().toRupiah()+" Change"
        }

        mBinding.total.text = "out of "+args.total.toString().toRupiah()

        mBinding.btnEmail.setOnClickListener {
            val text =
                "Total belanja anda ${args.total.toString().toRupiah()}.\n" +
                        "Total kembalian anda ${jml.toString().toRupiah()}.\n" +
                        "Terima kasih." // use country code with your phone number

            val mIntent = Intent(Intent.ACTION_SEND)
            mIntent.data = Uri.parse("mailto:")
            mIntent.type = "text/plain"
            mIntent.putExtra(Intent.EXTRA_SUBJECT, "Dalenta Apps")
            mIntent.putExtra(Intent.EXTRA_TEXT, text)
            startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
        }

        mBinding.btnWA.setOnClickListener {
            SosmedWA().whatApps(requireContext(), "+628989500790", args.total.toString().toRupiah(), jml.toString().toRupiah())
        }
    }
}