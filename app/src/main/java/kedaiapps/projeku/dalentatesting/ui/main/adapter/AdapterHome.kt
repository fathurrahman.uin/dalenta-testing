package kedaiapps.projeku.dalentatesting.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kedaiapps.projeku.dalentatesting.R
import kedaiapps.projeku.dalentatesting.databinding.ItemHomeBinding
import kedaiapps.projeku.dalentatesting.ext.inflate
import kedaiapps.projeku.dalentatesting.ext.toRupiah
import kedaiapps.projeku.dalentatesting.helper.DataHome

class AdapterHome (
    private val onClick:(DataHome) -> Unit
) : RecyclerView.Adapter<AdapterHome.ViewHolder>() {

    var items: MutableList<DataHome> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_home))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as? ViewHolder)?.bind(items.getOrNull(position)!!)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(containerView: View) :RecyclerView.ViewHolder(containerView){
        private var binding = ItemHomeBinding.bind(containerView)

        fun bind(data: DataHome){
            with(binding){
                parent.setOnClickListener {
                    onClick(data)
                }

                Glide.with(itemView.context).load(data.image)
                    .apply(
                        RequestOptions()
                            .skipMemoryCache(true)
                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                            .placeholder(R.drawable.dummy_menu)
                            .dontAnimate()
                    ).into(binding.ivImage)

                tvName.text = data.name
                tvPrice.text = data.price.toString().toRupiah()
            }
        }
    }

    fun insertData(data: List<DataHome>){
        data.forEach {
            items.add(it)
            notifyDataSetChanged()
        }
    }

    fun clearData() {
        if (items.isNotEmpty()) {
            items.clear()
            notifyDataSetChanged()
        }
    }

    fun setFilter(dataFilter: List<DataHome>) {
        dataFilter.forEach {
            items.add(it)
            notifyDataSetChanged()
        }
    }
}