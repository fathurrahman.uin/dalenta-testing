package kedaiapps.projeku.dalentatesting.ui.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.app.ActivityCompat
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kedaiapps.projeku.dalentatesting.databinding.FragmentCheckoutBinding
import kedaiapps.projeku.dalentatesting.ext.toRupiah
import kedaiapps.projeku.dalentatesting.modules.base.BaseFragment
import kedaiapps.projeku.dalentatesting.viewmodel.MainViewModel
import kotlin.math.ceil
import kotlin.math.round

class CheckoutFragment : BaseFragment() {

    lateinit var mBinding: FragmentCheckoutBinding
    private val args by navArgs<CheckoutFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentCheckoutBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initToolbar()
        initView()
    }

    private fun initToolbar(){
        mBinding.line.setOnClickListener { findNavController().popBackStack() }

        activity?.onBackPressedDispatcher?.addCallback(requireActivity(),
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    findNavController().popBackStack()
                }
            }
        )
    }

    private fun initView() {

        val data = (args.total/1000).toString().last()
        val number = Character.getNumericValue(data)

        val total: Double = (args.total/1000).toDouble()
        val total1: Double = ceil((total+1)*1000)

        var total2:Double
        if(number <4){
            val sel = 5-number
            total2 = (total+sel)*1000
        }else{
            total2 = (ceil((total+1)/10)*10000)
        }

//        val total2: Double = (ceil(((total+1)/10))*10000)
        val total3: Double = total2+5000.toDouble()
        mBinding.total.text = total1.toString().toRupiah()
        mBinding.option1.text = total1.toString().toRupiah()
        mBinding.option2.text = total2.toString().toRupiah()
        mBinding.option3.text = total3.toString().toRupiah()

        mBinding.option1.setOnClickListener {
            findNavController().navigate(CheckoutFragmentDirections
                .actionCheckoutFragmentToReceiptFragment(total1.toInt(), total1.toInt()))
        }

        mBinding.option2.setOnClickListener {
            findNavController().navigate(CheckoutFragmentDirections
                .actionCheckoutFragmentToReceiptFragment(total1.toInt(), total2.toInt()))
        }

        mBinding.option3.setOnClickListener {
            findNavController().navigate(CheckoutFragmentDirections
                .actionCheckoutFragmentToReceiptFragment(total1.toInt(), total3.toInt()))
        }
    }
}