package kedaiapps.projeku.dalentatesting.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import kedaiapps.projeku.dalentatesting.R
import kedaiapps.projeku.dalentatesting.databinding.ActivityMainBinding
import kedaiapps.projeku.dalentatesting.modules.base.BaseActivity

class MainActivity : BaseActivity() {

    lateinit var mBinding: ActivityMainBinding

    // --- untuk back to close app ---
    private var mRecentlyBackPressed = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mBinding.root)

        val navController = findNavController(R.id.navHost)
        navController.setGraph(R.navigation.nav_graph)
    }

//    override fun onBackPressed() {
//        val navController = findNavController(R.id.navHost)
//
//        if(navController.graph.startDestination == navController.currentDestination?.id){
//            if (mRecentlyBackPressed) {
//                ActivityCompat.finishAffinity(this)
//            } else {
//                toast("Tekan sekali lagi untuk keluar")
//                mRecentlyBackPressed = true
//            }
//        }else {
//            super.onBackPressed()
//            mRecentlyBackPressed = false
//        }
//    }
}