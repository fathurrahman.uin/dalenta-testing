package kedaiapps.projeku.dalentatesting.ui.main.adapter

import android.util.Log
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import kedaiapps.projeku.dalentatesting.R
import kedaiapps.projeku.dalentatesting.databinding.ItemDialogHomeBinding
import kedaiapps.projeku.dalentatesting.ext.inflate
import kedaiapps.projeku.dalentatesting.ext.toRupiah
import kedaiapps.projeku.dalentatesting.helper.DataHomeSize

class AdapterHomeSize (
    private val onClick:(DataHomeSize) -> Unit
) : RecyclerView.Adapter<AdapterHomeSize.ViewHolder>() {

    var items: MutableList<DataHomeSize> = ArrayList()

    var mSelected = -1
    var mSize = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(parent.inflate(R.layout.item_dialog_home))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        (holder as? ViewHolder)?.bind(items.getOrNull(position)!!)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class ViewHolder(containerView: View) : RecyclerView.ViewHolder(containerView){
        private var binding = ItemDialogHomeBinding.bind(containerView)

        fun bind(data: DataHomeSize){
            with(binding){
//                line.setOnClickListener {
//                    onClick(data)
//                }

                if(mSize != "") {
                    binding.pilih.isChecked = mSize == data.title
                }else {
                    binding.pilih.isChecked = mSelected == position
                }

                tvProduct.text = data.title
                tvPrice.text = data.price.toString().toRupiah()

                binding.pilih.setOnClickListener { _->
                    mSize = data.title
                    onClick(data)
                    mSelected = adapterPosition
                    notifyDataSetChanged()
                }
            }
        }
    }

    fun insertData(data: List<DataHomeSize>){
        data.forEach {
            items.add(it)
            notifyDataSetChanged()
        }
    }

    fun clearData() {
        if (items.isNotEmpty()) {
            items.clear()
            notifyDataSetChanged()
        }
    }
}