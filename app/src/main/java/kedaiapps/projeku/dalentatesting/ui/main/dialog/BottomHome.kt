package kedaiapps.projeku.dalentatesting.ui.main.dialog

import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.hilt.android.AndroidEntryPoint
import kedaiapps.projeku.dalentatesting.R
import kedaiapps.projeku.dalentatesting.databinding.DialogHomeBinding
import kedaiapps.projeku.dalentatesting.helper.DataHome
import kedaiapps.projeku.dalentatesting.helper.DataHomeSize
import kedaiapps.projeku.dalentatesting.ui.main.adapter.AdapterHomeSize
import kedaiapps.projeku.dalentatesting.viewmodel.MainViewModel

@AndroidEntryPoint
class BottomHome(
    private val data: DataHome
) : BottomSheetDialogFragment() {

    lateinit var mBinding: DialogHomeBinding
    private val viewModelMain by viewModels<MainViewModel>()

    private val adapter by lazy(LazyThreadSafetyMode.NONE){
        AdapterHomeSize(::onClick)
    }

    private var jml = 1
    private var size = ""
    private var sizeprice = 0
    private var service = true
    private var tax = true

    interface Listener {
        fun onYes()
    }

    var listener : Listener?= null

    override fun getTheme() = R.style.CustomBottomSheetDialogTheme

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = DialogHomeBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = BottomSheetDialog(requireContext(), theme)
        dialog.behavior.state = BottomSheetBehavior.STATE_EXPANDED
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        handleState()
    }

    private fun initView() {
        mBinding.tvProduct.text = data.name
        mBinding.rvSize.adapter = adapter

        mBinding.btnPlus.setOnClickListener {
            jml += 1
            mBinding.qty.text = jml.toString()
        }

        mBinding.btnMin.setOnClickListener {
            jml -= 1
            if (jml < 1) {
                jml = 1
            }
            mBinding.qty.text = jml.toString()
        }

        mBinding.pilihService.setOnCheckedChangeListener { _, b ->
            service = b
        }

        mBinding.pilihTax.setOnCheckedChangeListener { _, b ->
            tax = b
        }

        mBinding.btnAdd.setOnClickListener {

            viewModelMain.getDalentaBySize(data.id, size).observe(viewLifecycleOwner, {
                if (it != null){
                    jml += it.qty
                    val total = ((data.price + sizeprice) * jml)
                    viewModelMain.updateDalenta(data.id, jml, size, sizeprice, mBinding.tvNote.text.toString(), total, service, tax)

                    dismiss()
                    listener?.onYes()
                } else {
                    val total = data.price + sizeprice
                    viewModelMain.insertDalenta(
                        data.id,
                        data.name,
                        data.price,
                        jml,
                        size,
                        sizeprice,
                        mBinding.tvNote.text.toString(),
                        total,
                        service,
                        tax
                    )

                    dismiss()
                    listener?.onYes()
                }
            })

        }

        mBinding.ivClose.setOnClickListener {
            dismiss()
        }
    }

    private fun handleState() {

        adapter.clearData()
        adapter.insertData(DataHomeSize().getData())
    }

    private fun onClick(it: DataHomeSize) {
        size = it.title
        sizeprice = it.price
    }
}