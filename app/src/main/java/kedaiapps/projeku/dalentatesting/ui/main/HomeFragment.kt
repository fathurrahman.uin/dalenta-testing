package kedaiapps.projeku.dalentatesting.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import kedaiapps.projeku.dalentatesting.databinding.FragmentHomeBinding
import kedaiapps.projeku.dalentatesting.db.table.DalentaTable
import kedaiapps.projeku.dalentatesting.ext.hideKeyboard
import kedaiapps.projeku.dalentatesting.ext.toRupiah
import kedaiapps.projeku.dalentatesting.helper.DataHome
import kedaiapps.projeku.dalentatesting.modules.base.BaseFragment
import kedaiapps.projeku.dalentatesting.ui.main.adapter.AdapterHome
import kedaiapps.projeku.dalentatesting.ui.main.adapter.AdapterHomeProduct
import kedaiapps.projeku.dalentatesting.ui.main.dialog.BottomHome
import kedaiapps.projeku.dalentatesting.ui.main.dialog.BottomHomeEdit
import kedaiapps.projeku.dalentatesting.viewmodel.MainViewModel
import kotlin.math.ceil

class HomeFragment : BaseFragment() {

    lateinit var mBinding: FragmentHomeBinding
    private val viewModelMain by viewModels<MainViewModel>()

    private val adapterHome by lazy(LazyThreadSafetyMode.NONE) {
        AdapterHome(::onClick)
    }

    private val adapterProduct by lazy(LazyThreadSafetyMode.NONE) {
        AdapterHomeProduct(::onClickProduct, ::onClickProductDel)
    }

    private var list: List<DataHome> = ArrayList()

    var subtotal = 0
    var service = 0
    var tax = 0
    var totaltax = 0
    var totalgrand = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mBinding = FragmentHomeBinding.inflate(inflater, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        handleState()
    }

    private fun initView() {

        viewModelMain.getDalenta()

        mBinding.rvHome.adapter = adapterHome
        mBinding.tvProduct.adapter = adapterProduct

        mBinding.search.addTextChangedListener {
            val filteredModelList: List<DataHome> = filter(list, it.toString())
            adapterHome.clearData()
            adapterHome.setFilter(filteredModelList)
        }

        mBinding.search.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                hideKeyboard()
                return@setOnEditorActionListener true
            }
            false
        }

        mBinding.btnSubmit.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToCheckoutFragment(totalgrand))
        }

    }

    private fun handleState() {

        adapterHome.clearData()
        adapterHome.insertData(DataHome().getData())

        list = DataHome().getData()

        viewModelMain.getDalenta().observe(viewLifecycleOwner, {
            if (it.isNotEmpty()){
                adapterProduct.clearData()
                adapterProduct.insertData(it)

                setData(it)
            } else {
                adapterProduct.clearData()
            }
        })
    }

    private fun setData(it: List<DalentaTable>?) {

        subtotal=0
        service=0
        tax=0

        it?.forEach {
            subtotal += it.total

            if(it.service){
                service += (it.total*5)/100
            }

            if(it.tax){
                tax += (it.total*10)/100
            }
        }

        mBinding.subtotal.text = subtotal.toString().toRupiah()

        totaltax = service+tax
        mBinding.tax.text = totaltax.toString().toRupiah()

        totalgrand = subtotal+totaltax
        mBinding.grandtotal.text = totalgrand.toString().toRupiah()

        mBinding.btnSubmit.text = "Charge "+totalgrand.toString().toRupiah()
    }

    private fun onClick(it: DataHome) {
        val bottomPayment = BottomHome(it)
        bottomPayment.listener = object : BottomHome.Listener{
            override fun onYes() {
                viewModelMain.getDalenta()
            }
        }
        bottomPayment.show(requireActivity().supportFragmentManager, "Bottom Add Menu")
    }

    private fun onClickProduct(it: DalentaTable) {
        val bottomPayment = BottomHomeEdit(it)
        bottomPayment.listener = object : BottomHomeEdit.Listener{
            override fun onYes() {
                viewModelMain.getDalenta()
            }
        }
        bottomPayment.show(requireActivity().supportFragmentManager, "Bottom Add Menu")
    }

    private fun onClickProductDel(it: DalentaTable) {
//        val dialog = DialogConfirmation(requireActivity(), "Apakah Anda yakin ?")
//        dialog.listener = object: DialogConfirmation.Listener{
//            override fun onYes() {
//                viewModelMain.cartDelete(args.tableid, it.detailid)
//            }
//        }
//        dialog.show()

        viewModelMain.deleteById(it.id)
        viewModelMain.getDalenta()
    }

    private fun filter(
        models: List<DataHome>,
        query: String
    ): List<DataHome> {

        val filteredModelList: MutableList<DataHome> = ArrayList()
        for (model in models) {
            val name: String = model.name.toLowerCase()
            val price: String = model.price.toString().toLowerCase()
            if (name.contains(query.toLowerCase()) || price.contains(query.toLowerCase())) {
                filteredModelList.add(model)
            }
        }
        return filteredModelList
    }
}