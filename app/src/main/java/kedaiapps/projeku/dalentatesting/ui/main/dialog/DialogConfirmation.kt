package kedaiapps.projeku.dalentatesting.ui.main.dialog

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import kedaiapps.projeku.dalentatesting.databinding.PopupKonfirmasiBinding

class DialogConfirmation (val context: Activity, val message: String): Dialog(context) {

    lateinit var mBinding: PopupKonfirmasiBinding
    interface Listener {
        fun onYes()
    }

    var listener : Listener?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        mBinding = PopupKonfirmasiBinding.inflate(LayoutInflater.from(getContext()))
        setContentView(mBinding.root)

        window?.setBackgroundDrawableResource(android.R.color.transparent)
        window?.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT)

        setCanceledOnTouchOutside(false)
        setCancelable(false)

        mBinding.message.text = message
        mBinding.btnYes.setOnClickListener {
            listener?.onYes()
            dismiss()
        }

        mBinding.btnNo.setOnClickListener {
            dismiss()
        }
    }
}