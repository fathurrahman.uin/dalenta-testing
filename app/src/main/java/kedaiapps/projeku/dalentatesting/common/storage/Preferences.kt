package kedaiapps.projeku.dalentatesting.common.storage

import android.content.SharedPreferences
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Preferences @Inject constructor(private val prefs: SharedPreferences) {

    var status: String by PreferenceData(prefs, "status", "")

    var userid: String by PreferenceData(prefs, "userid", "")
    var token: String by PreferenceData(prefs, "token", "")
    var merchantid: String by PreferenceData(prefs, "merchantid", "")
    var merchant: String by PreferenceData(prefs, "merchant", "")
    var userlevel: String by PreferenceData(prefs, "userlevel", "")

    var printer: Int by PreferenceData(prefs, "printer", -1)
    var printername: String by PreferenceData(prefs, "printername", "")

    fun clear() {
        prefs.edit().clear().apply()
    }
}